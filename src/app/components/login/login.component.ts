import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { VendedorI, VendedorLoginI } from 'src/app/interfaces/vendedor.interface';
import { VendedorService } from 'src/app/servicios/vendedor.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    
  mensaje!: string;
  mensaje1: string = 'Ingresado';
  noenviado: string = 'Correo o contraseña incorrecta'
  propiedades: any = {
    error: false
  }

  formulario!: FormGroup;
  loading: boolean = false;


  constructor(private fb: FormBuilder,
              private _vendedorSVC: VendedorService,
              private router: Router) { 
    this.crearFormulario()
  }


  get correo1NoValido(){
    return this.formulario.get('correo1')?.invalid && this.formulario.get('correo1')?.touched
  }

  get contraseniaNoValido(){
    return this.formulario.get('contrasenia')?.invalid && this.formulario.get('contrasenia')?.touched
  }

  crearFormulario(): void {
    // console.log('Form');

    this.formulario = this.fb.group({
      correo1: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      contrasenia: ['', [Validators.required, Validators.minLength(4)]],
      // recuerdame: ['']
    });
  }

  // variable con toda la informacion
  readData:any;

  ngOnInit(): void {
  }

  ingresar(){
    console.log('Cargando');

    console.log(this.formulario.value);
    const Vendedor: VendedorLoginI = {
      correo1: this.formulario.value.correo1,
      contrasenia: this.formulario.value.contrasenia
    }

    // console.log(Vendedor);
    // console.log(this._vendedorSVC.buscarCuenta(Vendedor));
    
    // this._vendedorSVC.buscarCuenta(Vendedor)

    this.getAllData(this.formulario.value.correo1, this.formulario.value.contrasenia)



  }

  getAllData(correo: string, contrasenia: string){
    
    this._vendedorSVC.getAllData().subscribe((res)=>{
      // console.log(res, "res==>");
      this.readData = res.data;    
      console.log(this.readData); //TODOS LOS VENDEDORES

      for (let i = 0; i < this.readData.length; i++){
        // console.log(this.readData[i]);//LOG DE CADA VENDEDOR  

        if(this.readData[i].correo1 === correo && this.readData[i].contrasenia === contrasenia){

          this.fakeLoading();

          console.log(this.readData[i],'info del vendedor logeado');
          const here = this.readData[i];
          let gID = here.id_vendedor
          console.log(here);
          console.log(gID);
          
          
          
          this._vendedorSVC.loginVendedor(this.readData[i], gID)

        } else if (this.readData[i].correo1 ==! correo) {

          this.mensaje = this.mensaje1;
          setTimeout(() => {
            this.mensaje = '';
          }, 4000);
    
        this.propiedades.error = true;
        setTimeout(() => {
          this.propiedades.error =false;
        }, 4000);

        } else {
          this.mensaje = this.noenviado;
          setTimeout(() => {
            this.mensaje = '';
          }, 4000);
    
        this.propiedades.error = true;
        setTimeout(() => {
          this.propiedades.error =false;
        }, 4000);

        this.formulario.reset();
        }
      }
    });
  }

  fakeLoading():void{
    this.loading = true;
    // console.log('fakeloading');
    
    setTimeout(() => {
      this.loading = false;
      //Redireccionamos al dashboard
      // this.router.navigate(['index'])
    }, 3000);
  }


}
