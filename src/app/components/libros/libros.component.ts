import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoriasService } from 'src/app/servicios/categorias.service';
import { LibroService } from 'src/app/servicios/libro.service';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.css']
})
export class LibrosComponent implements OnInit {

  formato:any;

  constructor(private libroSVC: LibroService,
              private categoriaSVC: CategoriasService,
              private router: Router) {
    
    this.Categoria();

    this.Formato();
  }

  ngOnInit(): void {
    console.log(this.libroSVC);
    
  }

  readCategoria: any;
  readFormato: any;

  Categoria(){
    this.categoriaSVC.getAllData().subscribe((res)=>{
      console.log(res, "res==>");
      this.readCategoria = res.data;
    });
  }

  Formato(){
    this.formato = this.libroSVC.getFormato()
  }

  Libros(){
    this.categoriaSVC.getAllData().subscribe((res)=>{
      console.log(res, "res==>");
      this.readCategoria = res.data;
    });
  }
  

  buscarLibro(termino: string): void{
    console.log(termino);
    
    this.router.navigate(['/buscar', termino]); //al apretar enter o el boton, aqui ira al componente de buscar, y el termino como pusimos en routes, y entra a la clase buscadorComponente
  }


}
