import { ThisReceiver } from '@angular/compiler';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoriasService } from 'src/app/servicios/categorias.service';
import { LibroService } from 'src/app/servicios/libro.service';
import { VendedorService } from 'src/app/servicios/vendedor.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {


  @ViewChild('fileInput', { static: false }) fileInput!: ElementRef

  idioma: string[] = ['Español', 'Inglés'];
  formulario!: FormGroup

  enviado: string = 'Registro finalizado con éxito';
  mensaje!: string;

  mostrar = false;
  userName!: number;

  file!: File;
  
  propiedades: any = {
    error: false
  }

  constructor(private _librosSVC: LibroService,
              private _vendedorSVC :VendedorService,
              private _categoriaSVC: CategoriasService,
              private fb: FormBuilder) {
    this.crearFormulario();
    this.getAll();
    this.getCategoria();
    
    let nombre = sessionStorage.getItem('ID')
    if (nombre === null){
      this.userName = 0
    } else {
      this.userName = parseInt(nombre)
      console.log(this.userName, 'parseInt');
      this.getPerfil()
      
      this.mostrar = true
    }

        console.log(nombre, 'nombre');

        
   }


   crearFormulario(){
    this.formulario = this.fb.group({
      idioma: ['', Validators.required],
      titulo: ['', [Validators.required, Validators.minLength(3)]],
      subtitulo: ['', [Validators.minLength(3)]],
      autor_nombre: ['', [Validators.required, Validators.minLength(3)]],
      autor_apellido: ['', [Validators.required, Validators.minLength(3)]],
      descripcion: ['', [Validators.required, Validators.minLength(3)]],
      precio: ['', Validators.required],
      formato: ['', Validators.required],
      fecha: ['', Validators.required],
      imagen: ['', Validators.required],
      id_categoria: ['', Validators.required],
    })
   } 

  ngOnInit(): void {
  }

  readData: any;
  readCategoria: any;
  readPerfil: any;

  getAll(){
    this._librosSVC.getAllData().subscribe((res)=>{
      console.log(res, "res==>");
      this.readData = res.data;    
    });
  }


  guardar(){
    console.log(this.formulario.value);

      if(this.formulario.valid){

        
        console.log(this.formulario.value);
        this._librosSVC.createData(this.formulario.value).subscribe((res)=>{
          console.log(res, 'res==>');
        });
  
        this.mensaje = this.enviado;
        setTimeout(() => {
          this.mensaje = '';
        }, 4000);
  
      this.propiedades.error = true;
      setTimeout(() => {
        this.propiedades.error =false;
      }, 4000);
  }
}


  getCategoria(){
    
    this._categoriaSVC.getAllData().subscribe((res)=>{
      console.log(res, "res==>");
      this.readCategoria = res.data;
    });
  }

  cerrarForm(){
    console.log('close');
  }


  onSelect(event: any) {
    this._librosSVC.onSelect(event)
  }

  onFileUpload(){
console.log('file upload');

    this._librosSVC.onFileUpload(this.fileInput)
  }


  getPerfil(){
    console.log('getPerfil');

    console.log(this.userName, 'para la base de datos');
    
    
    this._vendedorSVC.getOneData(this.userName).subscribe((res)=>{
      console.log('ojala');
      
      console.log(res, "res==>");
      this.readPerfil = res.data;
    });
  }

}
