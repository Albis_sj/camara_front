import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LibroI } from 'src/app/interfaces/libro.interface';
import { LibroService } from 'src/app/servicios/libro.service';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {

  libros: any ;
  termino!: string;

  constructor(private _librosSVC: LibroService,
              private router: Router, 
              private activatedRoute: ActivatedRoute,) { }

  ngOnInit(): void {
    this.getAllData();

    this.activatedRoute.params.subscribe(params => { //subscribe obtiene una variable
      console.log(params['termino'],'Init del termino');
      this.termino = params['termino']; //se lo guarda en una variables lo que hace log, y esa variable 
      console.log(this.termino, 'termino = params');
      
      //NO PASA DE AQUI

      this.libros = this._librosSVC.buscarLibro(this.termino.toLowerCase());

      console.log(this.libros, 'lista de libros');
    })
  }

  readData:any;
  //get Data
  getAllData(){
    console.log('get data');

    this._librosSVC.getAllData().subscribe((res)=>{
      console.log(res, "res==>");
      this.readData = res.data;
    });
  }

  


}
