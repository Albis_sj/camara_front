import { Component, OnInit } from '@angular/core';
import { VendedorI } from 'src/app/interfaces/vendedor.interface';
import { VendedorService } from 'src/app/servicios/vendedor.service';

@Component({
  selector: 'app-editoriales',
  templateUrl: './editoriales.component.html',
  styleUrls: ['./editoriales.component.css']
})
export class EditorialesComponent implements OnInit {

  ListaConFiltro:VendedorI[] = [];

  constructor(private _vendedorSVC : VendedorService) {

    this.getAll();
    console.log('get All');
    
   }

  readData:any;

  ngOnInit(): void {
  }

  getAll(){
    this._vendedorSVC.getAllData().subscribe((res)=>{
      console.log('AAAAAAAAAAAAAAAAAA');
      
      console.log(res, "res==>");
      this.readData = res.data;    
      console.log(this.readData);

      for (let i = 0; i < this.readData.length; i++){
        console.log(this.readData[i]);
        if(this.readData[i].categoria === 'Editorial'){
          console.log('juju');

          this.ListaConFiltro.push(this.readData[i])
          console.log(this.ListaConFiltro, 'aasa');
        }
      }
    });
  }

}
