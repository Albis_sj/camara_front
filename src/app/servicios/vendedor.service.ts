import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { VendedorI, VendedorLoginI } from '../interfaces/vendedor.interface';
import { Router } from '@angular/router';



@Injectable({
  providedIn: 'root'
})
export class VendedorService {

  user!: VendedorI;
  ID_user!: string;

  constructor(private _http: HttpClient,
              private router: Router) { }

  apiUrl = 'http://localhost:3000/vendedor';
  Vendedor!: VendedorLoginI;


  //get all data
  getAllData(): Observable<any>{
    return this._http.get(`${this.apiUrl}`);
  }


    //get all data
    getOneData(id: number): Observable<any>{
      console.log('getOneData');
      
      return this._http.get(`${this.apiUrl}/${id}`);
    }

  //create data
  createData(data:any,):Observable<any>{
    console.log(data, 'create Api =>');

    return this._http.post(`${this.apiUrl}`, data);
  }



    //LOGIN DATOS
    loginVendedor(user: VendedorI, id:number){
     this.ID_user = id.toString()
      // console.log(user);
      this.user = user;
      // console.log(this.user);
      sessionStorage.setItem('ID', this.ID_user);
      // console.log('subido al session storage');
      let nombre = sessionStorage.getItem('ID');
      
      this.router.navigate([ '/dashboard/perfil', id ])
      // console.log(nombre, 'get');
      return  nombre;
    }

}
