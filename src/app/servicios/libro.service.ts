import { HttpClient } from '@angular/common/http';
import { ElementRef, Injectable } from '@angular/core';
import { Observable, retry } from 'rxjs';
import { LibroI } from '../interfaces/libro.interface';

@Injectable({
  providedIn: 'root'
})
export class LibroService {

  formato = [
    { id_formato: 1, formato: 'Libro de bolsillo'},
    { id_formato: 2, formato: 'De tapa dura'},
    { id_formato: 3, formato: 'Letra grande'},
    { id_formato: 4, formato: 'Audiolibro audible'},
    { id_formato: 5, formato: 'Código de acceso impreso'},
    { id_formato: 6, formato: 'Hoja suelta'},
    { id_formato: 7, formato: 'CD de audio'},
    { id_formato: 8, formato: 'Libro de cartón'},
    { id_formato: 9, formato: 'Encuadernado en espiral'},
  ]

  file!: File;
  libro!: LibroI;
  ListaLibro: LibroI[] = [];


  

  constructor(private _http: HttpClient) { 

    // this.getLibros();

    this.getAllD();
  }

  apiUrl = 'http://localhost:3000/libros';

  getAllData(): Observable<any>{
    return this._http.get(`${this.apiUrl}`);
  }

  createData(data:any):Observable<any>{
    console.log(data, 'create Api =>');
    return this._http.post(`${this.apiUrl}`, data);
  }

  getFormato(){
    return this.formato
  }

  readData: any;
  
  getAllD(){
    this.getAllData().subscribe((res)=>{
      // console.log(res, "res==>");
      this.readData = res.data;    
    });
  }
  
  //get Data
  getLibros(){
    this.getAllData().subscribe((res)=>{
      console.log(res, "res==>");
      this.readData = res.data;
      this.ListaLibro = res.data;
      console.log(this.ListaLibro, 'get Libros');
    });
  }

  buscarLibro(termino: string){
    
  }


  // SUBIR LIBRO
  onSelect(event: any) {
    this.file = event.target.files[0]

    if(event.target.files[0]){
      let reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event: any) => {
        this.apiUrl = event.target.result
      }
    }
  }
  
  onFileUpload(fileInput : ElementRef){

    const imageBlob = fileInput.nativeElement.files[0];
    const file = new FormData();

    file.set('file', imageBlob);

    this._http.post(`${this.apiUrl}`, file).subscribe(res => {
      console.log(res);
    });
  }



}




