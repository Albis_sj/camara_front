export interface LibroI{
  idioma: string,
  titulo: string,
  subtitulo: string,
  autor_nombre: string,
  autor_apellido: string,
  descripcion: string,
  precio: number,
  fecha: Date,
  formato: string,
  id_editorial: number,
  id_categoria: number,
}