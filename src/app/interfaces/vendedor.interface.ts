export interface VendedorI {
  nombre: string,
  categoria: string,
  correo1: string,
  contrasenia: string,
  telefono1: string,
  telefono2: string,
  celular1: number,
  celular2: string,
  correo2: number,
  direccion: string,
  pagina: string
};

export interface VendedorLoginI {
  correo1: string,
  contrasenia: string
}